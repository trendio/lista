<?php

namespace Trendio;

class Lista
{

    private static $_instance;

    /**
     * Store the current query params
     *
     * @var array
     */
    protected static $queryParams = [];

    /**
     * The current active columns
     *
     * @var array
     */
    protected static $activeColumns = [];

    /**
     * Id for the table currently being displayed
     * @var string
     */
    public static $tableId = 'datatable';

    /**
     * Row formatter should be an anonymous function that returns a <tr> tag
     *
     * Lista::$rowFormatter = function($data) {
     *      return '<tr class="awesome">';
     * };
     *
     * @var callable
     */
    public static $rowFormatter;


    /**
     * Defines a custom sorting option for columns
     *
     * Lista::$customSorting = [
     *      'column' => function($row, $value, $data) {
     *         return ;
     *      }
     * ]
     *
     * @var array of callables
     */
    public static $customSorting = [];

    /**
     * Extra classes that can be passed to the table.
     * Note that we are using bootstrap for table formatting
     *
     * @var array
     */
    public static $tableClasses = ['table-striped'];

    /**
     * Columns that you do not want to automatically wrap.
     *
     * Lista::$noWrap = ['columnkey1', 'columnkey2'];
     *
     * @var array
     */
    public static $noWrap = [];

    /**
     * Columns that you do not want to center
     *
     * Lista::$center = ['columnkey1', 'columnkey2'];
     *
     * @var array
     */
    public static $center = [];

    /**
     * Filter By
     *
     * @var array
     */
    public static $filters = [];

    /**
     * Is in printable view
     *
     * @var boolean
     */
    public static $print = false;

    /**
     * Set if you want to render as exportable
     *
     * @var boolean
     */
    public static $export = false;

    public static $sort = true;

    public static $emptyDefaulValue = '--';

    /**
     * The default delimiter for export
     *
     * @var string
     */
    public static $exportDefaultDelimiter = "\t";

    /**
     * If the hide columns dropdown should be displayed
     *
     * @var boolean
     */
    public static $filterColumns = false;

    public static $csrfField = '';

    /**
     * Message displayed if no records available
     *
     * @var string
     */
    public static $noRecordsMessage = 'No records available';

    /**
     * Instance of Symfony request
     *
     * @var Symfony\Component\HttpFoundation\Request
     */
    protected static $request;

    /**
     * Array pagination
     *
     * @var array
     */
    protected static $paginate = [];
    public static $paginationView = null;

    protected static $sortDefaultDirection = 'asc';
    protected static $sortDefaultColumn = '';


    /**
     * Using as livewire table
     *
     * @var boolean
     */
    public static $liveWire = false;


    public static function getInstance ()
    {
        if (empty(self::$_instance)) {
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    /**
     * Returns the rendered html table for display
     *
     * @param array $data
     * @param array $columns
     * @param array $formatters
     * @return string
     */
    public static function render($data, $columns, $formatters = [])
    {

        if (empty(self::$queryParams)) {
            //if query parameters do not exist. then lets initialize them
            self::queryParams();
        }

        if (self::$export) {
            //render as exportable
            //@todo In future iterations the render should be an engine. You could pass the rendering engine
            return self::renderExport($data, $columns, $formatters = []);
        }

        $pagination = null;

        $html = '';
        if (!empty($data)) {
            $html .= self::renderColumnSelector($columns);
        }

        if (!empty(self::$paginate)) {
            $data = new \Illuminate\Pagination\LengthAwarePaginator($data, self::$paginate['total'], self::$paginate['perPage'], self::$paginate['currentPage'], self::$paginate['options']);
            $data->setPath(self::paginationUrl());
        }

        $html .= '<div class="lista">';

        //handle pagination stuff, if record set is a pagination instance
        if ($data instanceof \Illuminate\Pagination\LengthAwarePaginator) {
            $pagination = $data;
            $pagination->appends(self::requestParams())->links(self::$paginationView);
            $pagination->fragment(self::$tableId);
            //set the actual records
            $data = $data->items();

            //display paginated displayed records header
            $start = ($pagination->currentPage() - 1) * $pagination->perPage() + 1;
            $end = ($pagination->currentPage() - 1) * $pagination->perPage() + $pagination->count();
            if ($pagination->count() > 0) {
                $html .= '<div class="pages">';
                $html .= 'Showing '.$start.' - '.$end.' records of '.$pagination->total();
                $html .= '</div>';
            }
        }

        //reset if it's empty
        if ($data instanceof \Illuminate\Database\Eloquent\Builder && $data->count() === 0) {
            $data = [];
        }

        if (!empty($data)) {
            $html .= '<table id="'.self::$tableId.'" class="table ' . implode(' ', self::$tableClasses) . '">';
            $html .= self::header($columns);
            $html .= self::body($data, $columns, $formatters);
            $html .= '</table>';
        } else {
            $html .= '<div class="alert alert-warning text-center">';
            $html .= self::$noRecordsMessage;
            $html .= '</div>';
        }
        $html .= '</div>';

        if (!empty($pagination)) {

            //displays pagination
            $html .= $pagination->render(self::$paginationView);
        }

        return $html;
    }

    /**
     * Allows to attach filters to the query
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param array $filters The defined filters
     * @return \Illuminate\Database\Query\Builder
     */
    public static function filter($query, $filters = [])
    {
        if (empty($filters)) {
            $filters = self::$filters;
        }
        $requestKeys = array_keys($_REQUEST);

        foreach ($filters as $key => $value) {
            if (is_callable($value)) {
                $query = call_user_func($value, $query);
                continue;
            }
            $filterBy = '';
            $pieces = explode('.', $value);
            $fieldValue = array_pop($pieces);

            if (in_array($key, $requestKeys, true) !== false && isset($_REQUEST[$key])) {
                $filterBy = htmlspecialchars(trim($_REQUEST[$key]));
            } elseif (in_array($fieldValue, $requestKeys, true) !== false && isset($_REQUEST[$fieldValue])) {
                $filterBy = htmlspecialchars(trim($_REQUEST[$fieldValue]));
            }


            if (!isset($filterBy) || $filterBy === '') {
                continue;
            }

            if ($filterBy === 'NULL') {
                $query->whereNull($value);
            } elseif (is_string($key) && is_string($value)) {
                //filter with operator
                $query->where($key, $value, $filterBy);
            } else {
                //basic filter
                $query->where($value, $filterBy);
            }
        }

        return $query;
    }

    /**
     * Generates header html
     *
     * @param array $columns
     * @return string
     */
   protected static function header($columns)
    {
        $html = '<thead><tr>';
        foreach (self::getActiveColumns($columns) as $key => $col) {
            $sort = self::$sort;

            if (is_array($col)) {
                //trow error message if no value key exists
                $col = $col['value'];
                $sort = false;
            }

            $extraColData = '';
            if (!empty($columns[$key]['data'])) {

                $extraColData = collect($columns[$key]['data'])->map(function ($item, $ikey) {
                    return 'data-'.$ikey.'="'.$item.'"';
                })->values()->implode(' ');
            }
            $html .= '<th'.(in_array($key, self::$center) ? ' class="text-center"' : '').' data-col-key="'.$key.'" '.$extraColData.'>';
            if ($sort) {
               if (self::$liveWire) {
                    $direction = self::sortDirectionUrl();
                    $html .= self::$print ? '' : '<a href="#" wire:click="sort(\''.$key.'\',\''.$direction.'\')">';
               } else {
                    $html .= self::$print ? '' : '<a href="'.self::url($key).'">';
               }

            }

            $html .= $col;
            if ($sort && self::sortedColumn() == $key) {
                $html .= self::sortedIcon();
            }
            if ($sort) {
                $html .= self::$print ? '' : '</a>';
            }
            $html .= '</th>';
        }
        $html .= '</tr></thead>';
        return $html;
    }

    /**
     * Generates body html
     *
     * @param array $data
     * @param array $columns
     * @param array $formatters
     * @return string
     */
    protected static function body($data, $columns, $formatters = [])
    {
        $html = '<tbody>';
        if ($data instanceof \Illuminate\Database\Eloquent\Builder) {
            $data->chunk(50, function ($chunk) use (&$html, $columns, $formatters) {
                $html .= self::renderHtmlRows($chunk, $columns, $formatters);
            });
        } else {
            $html .= self::renderHtmlRows($data, $columns, $formatters);
        }

        return $html . '</tbody>';
    }

    /**
     * Generates an HTML table row
     *
     * @param array $data
     * @param array $columns
     * @param array $formatters
     * @return string
     */
    protected static function renderHtmlRows($data, $columns, $formatters)
    {
        $html = '';
        foreach ($data as $row) {
            $html .= self::renderHtmlRow($row, $columns, $formatters );
        }
        return $html;
    }

    public static function renderHtmlRow($row, $columns, $formatters)
    {
        $html = '';
        if (!empty(self::$rowFormatter)) {
            $html .= call_user_func(self::$rowFormatter, $row);
        } else {
            $html .= '<tr>';
        }

        foreach (self::getActiveColumns($columns) as $key => $col) {
            $nowrap = (in_array($key, self::$noWrap) ? ' nowrap' : '');
            $center = (in_array($key, self::$center) ? ' class="text-center"' : '');

            $sortCol = !empty($columns[$key]['sort_value']) ? 'data-order="'.self::getValue($row, $columns[$key]['sort_value']).'"' : '';

            $value = self::getValue($row, $key);

            if (!empty($formatters[$key]) && is_string($formatters[$key])) {
                $data = call_user_func($formatters[$key], $value);
            } elseif (!empty($formatters[$key])) {
                $data = call_user_func($formatters[$key], $row, $key);
            } else {
                $defaultEmptyValue = isset($columns[$key]['default_value']) ? $columns[$key]['default_value'] : self::$emptyDefaulValue;
                $data = empty($value) ? $defaultEmptyValue : $value;
            }

            $dataOrder = '';
            if (!empty(self::$customSorting[$key])) {
                $dataOrder = ' data-order="'.call_user_func(self::$customSorting[$key], $row, $value, $data).'"';
            }



            $html .= '<td'.$nowrap.$center.$dataOrder.' data-col-key="'.$key.'" class="col-'.$key.'" '.$sortCol.'>';
            $html .= $data;
            $html .= '</td>';
        }
        $html .= '</tr>';
        return $html;
    }

    protected static function getValue($data, $key) {
        $value = '';

        if (is_object($data) && isset($data->{$key})) {
            $value = $data->{$key};
        } elseif (is_array($data) && isset($data[$key])) {
            $value = $data[$key];
        }

        if (is_object($value) && get_class($value) == 'Illuminate\Support\Carbon')
        {
            return $value;
        }

        if (is_object($value)) {
            $value = implode(',', (array)$value);
        }

        return htmlentities($value);
    }

    /**
     * Render exportable version
     *
     * @param array $data
     * @param array $columns
     * @param array $formatters
     * @return string
     */
    protected static function renderExport($data, $columns, $formatters = [])
    {
        $body = implode(self::$exportDefaultDelimiter, array_values($columns));
        $body .= "\n";

        if ($data instanceof \Illuminate\Database\Eloquent\Builder) {
            $data->chunk(50, function ($chunk) use (&$body, $columns, $formatters) {
                $body .= self::renderExportRow($chunk, $columns, $formatters);
            });
        } else {
            $body .= self::renderExportRow($data, $columns, $formatters);
        }

        return $body;
    }

    /**
     * Render the active column selector
     *
     * @todo Only display after the number is greater than 5 columns?
     * @param array $columns
     * @return string
     */
    protected static function renderColumnSelector($columns)
    {

        if (self::$print || !self::$filterColumns) {
            return;
        }

        $active = self::getActiveColumns($columns);
        $html = '<form class="clearfix" method="post" action="'.self::paginationUrl().'">';
        $html .= self::$csrfField;
        $html .= '<div class="btn-group pull-right">';
        $html .= '<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">';
        $html .= 'Active Columns';
        $html .= '<span class="caret"></span>';
        $html .= '</a>';
        $html .= '<ul class="dropdown-menu columnselector">';
        if (count($columns) > 5) {
            $html .= '<li class="text-center">';
            $html .= '<button class="btn btn-mini" type="submit">Save</button>';
            $html .= '</li>';
        }
        foreach ($columns as $key => $value) {
            $html .= '<li style="padding:3px 6px">';
            $checked = !empty($active[$key]) ? 'checked' : '';
            $html .= '<input type="checkbox" name="colummns['.$key.']" '.$checked.'/> ';
            if (is_array($value)) {
                $html .= $value['value'];
            } else {
                $html .= $value;
            }

            $html .= '</li>';
        }
        $html .= '<li class="text-center">';
        $html .= '<button class="btn btn-mini" type="submit">Save</button>';
        $html .= '</li>';
        $html .= '</ul>';
        $html .= '</div>';
        $html .= '</form>';
        $html .= '<script>';
        $html .= '$(".columnselector li, .columnselector li input").click(function(e){e.stopPropagation();});';
        $html .= '</script>';
        return $html;
    }


    /**
     * Get the current active columns to display
     *
     * @param array $columns
     * @return array
     */
    protected static function getActiveColumns($columns)
    {
        $shaName = self::getCookieName($columns);
        if (!self::$filterColumns) {
            return $columns;
        } elseif (!isset($_COOKIE[$shaName])) {
            return self::processActiveColumns($shaName, $columns);
        } elseif (!empty($_POST['colummns'])) {
            return self::processActiveColumns($shaName, $columns, $_POST['colummns']);
        }

        if (empty(self::$activeColumns)) {
            self::$activeColumns = json_decode($_COOKIE[$shaName], true);
        }
        return self::$activeColumns;
    }

    /**
     * Process columns hide/show
     *
     * @param string $cookieName
     * @param array $columns
     * @return array
     */
    protected static function processActiveColumns($cookieName, $columns, $onColumns = null)
    {
        if (!empty($onColumns)) {
            $active = [];
            foreach ($onColumns as $key => $value) {
                $active[$key] = $columns[$key];
            }
            setcookie($cookieName, json_encode($active), mktime(0, 0, 0, date("m")+6, date("d"), date("Y")));
        } else {
            $active = $columns;
            if (!isset($_COOKIE[$cookieName])) {
                setcookie($cookieName, json_encode($active), mktime(0, 0, 0, date("m")+6, date("d"), date("Y")));
            }
        }

        self::$activeColumns = $active;
        return $active;
    }

    /**
     * Get unique string for the current Lista
     *
     * @param array $columns
     * @return string
     */
    protected static function getCookieName($columns)
    {
        //@todo cache name?
        return 'lista'.sha1(implode(',', array_keys($columns)));
    }

    /**
     * Render Export - If using Laravel then generate file and wrap in response
     *
     * @param \Illuminate\Database\Eloquent\Builder|array $data
     * @param array $columns
     * @param array $formatters
     * @return Symfony\Component\HttpFoundation\StreamedResponse|string
     */
    public static function export($data, $columns, $formatters=[])
    {
        if ($data instanceof \Illuminate\Database\Eloquent\Builder) {
            $response = new \Symfony\Component\HttpFoundation\StreamedResponse;

            $disposition = $response->headers->makeDisposition('attachment', 'export.csv');
            $response->headers->replace([
                'Content-Type' =>' text/csv',
                'Content-Disposition' => $disposition,
            ]);

            $response->setCallback(function ()  use ($data, $columns, $formatters) {
                $handle = fopen('php://output', 'w');
                 // Add CSV headers
                fputcsv($handle, $columns);
                $data->chunk(100, function ($chunk) use($handle, $columns, $formatters) {
                    foreach ($chunk as $row) {
                        $rowData=[];
                        foreach ($columns as $key => $col) {
                            $value = null;
                            if (is_object($row) && !empty($row->{$key})) {
                                $value = $row->{$key};
                            } elseif (is_array($row) && !empty($row[$key])) {
                                $value = $row[$key];
                            }
                            if (!empty($formatters[$key]) && is_string($formatters[$key])) {
                                $rowData[] = call_user_func($formatters[$key], $value);
                            } elseif (!empty($formatters[$key])) {
                                $rowData[] = call_user_func($formatters[$key], $row);
                            } else {
                                $rowData[] = $value;
                            }
                        }
                        fputcsv($handle, $rowData);
                    }
                });
                // Close the output stream
                fclose($handle);
            });
            return $response;
        } else {
            return self::renderExport($data, $columns, $formatters = []);
        }

    }

    /**
     * Generates exportable row
     *
     * @param array $data
     * @param array $columns
     * @param array $formatters
     * @return string
     */
    protected static function renderExportRow($data, $columns, $formatters)
    {
        $body = '';

        foreach ($data as $row) {
            $bodyData = [];
            foreach ($columns as $key => $col) {
                $value = null;
                if (is_object($row) && !empty($row->{$key})) {
                    $value = $row->{$key};
                } elseif (is_array($row) && !empty($row[$key])) {
                    $value = $row[$key];
                }
                if (!empty($formatters[$key]) && is_string($formatters[$key])) {
                    $bodyData[] = call_user_func($formatters[$key], $value);
                } elseif (!empty($formatters[$key])) {
                    $bodyData[] = call_user_func($formatters[$key], $row);
                } else {
                    $bodyData[] = $value;
                }
            }
            $body .= implode(self::$exportDefaultDelimiter, $bodyData)."\n";
        }

        return $body;
    }


    /**
     * Helper that saves all the current query params for url generation
     *
     * @return void
     */
    public static function queryParams()
    {
        $queryParams = [];
        foreach ($_REQUEST as $key => $value) {
            $queryParams[$key] = $value;
        }
        return self::$queryParams = $queryParams;
    }

    /**
     * Generates the Url of a column for sorting
     *
     * @param string $col
     * @return string
     */
    protected static function url($col)
    {
        $queryParams = self::$queryParams;
        $queryParams['direction'] = self::sortDirectionUrl();
        $queryParams['sort'] = $col;

        return self::getPath() . '?' . http_build_query($queryParams) . '#' . self::$tableId;
    }

    protected static function getPath()
    {
        if (!empty(self::$request)) {
            return '/'.self::$request->path();
        }

        return $_SERVER['SCRIPT_NAME'];
    }

    /**
     * Generate full URL with all query params
     *
     * @return string
     */
    public static function paginationUrl()
    {
        $queryParams = self::queryParams();
        return self::getPath() . '?' . http_build_query($queryParams);
    }

    /**
     * Returns the direction to sort
     *
     * @param string $default
     * @return string
     */
    public static function sortDirection($default = false)
    {
        if (!empty($default)) {
            self::$sortDefaultDirection = $default;
        } else {
            $default = self::$sortDefaultDirection;
        }
        if (self::$liveWire) {
            return $default;
        }

        return (!empty($_GET['direction']) ? htmlspecialchars($_GET['direction']) : $default);
    }

    /**
     * Returns the direction to sort
     *
     * @param string $default
     * @return string
     */
    public static function sortDirectionNew()
    {
        return self::sortDirection() == 'asc' ? 'up' : 'down';
    }

    /**
     * Returns the the opposite direction for url
     *
     * @return string
     */
    protected static function sortDirectionUrl()
    {
        return self::sortDirection() == 'asc' ? 'desc' : 'asc';
    }

    /**
     * Returns the icon to denote current sorted direction of a column
     *
     * @return string
     */
    protected static function sortedIcon()
    {
        return '&nbsp;&nbsp;<i class="fa fa-sort-'.self::sortDirectionNew().'" aria-hidden="true"></i>';
    }

    /**
     * Returns the current column being sorted
     *
     * @param string $defaultCol
     * @return string
     */
    public static function sortedColumn($defaultCol = '')
    {
        if (!empty($defaultCol) && empty(self::$sortDefaultColumn)) {
            self::$sortDefaultColumn = $defaultCol;
        } else {
            $defaultCol = self::$sortDefaultColumn;
        }
        if (self::$liveWire) {
            return $defaultCol;
        }
        return (!empty($_GET['sort']) ? htmlspecialchars($_GET['sort']) : $defaultCol);
    }

    /**
     * Set Symfony request
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public static function setRequest(\Symfony\Component\HttpFoundation\Request $request) {
        self::$request = $request;
    }

    /**
     * Helper that that returns sort/pagination params for external requests
     *
     * @return array
     */
    public static function requestParams() {
        return [
            'sort' => self::sortedColumn(),
            'direction' => self::sortDirection(),
            'page' => self::page()
        ];
    }

    /**
     * Get current page
     *
     * @return string
     */
    public static function page() {
        return (int)(!empty($_GET['page']) ? $_GET['page'] : 1);
    }

    /**
     * Enable pagination on arrays
     *
     * @param integer  $total
     * @param integer $perPage
     * @param array   $options
     * @return void
     */
    public static function paginate($total, $perPage = 25, array $options = []) {
        self::$paginate = [
            'total' => $total,
            'currentPage' => null,
            'perPage' => $perPage,
            'options' => $options
        ];
    }
}
